Steps to setup the project

1- clone this repo
2- rum "composer install"
3- copy env.example to .env 
4- set database , mail and stripe values
5- run "php artisan migrate"
6- run "php artiasn db:seed"
7- serve the project

Note : Queue listener should be in running mode in order to process emails and payments (php artisan queue:listen)