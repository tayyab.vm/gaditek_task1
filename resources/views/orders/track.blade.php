@extends('layouts.master')

@section('content')
<div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
    <img src="{{asset('images/order_success.png')}}" alt="" style="width:40%;margin:auto">
</div>
<div class="row">
    <h4 style="text-align: center">Order Status : {{$order->status}}</h4>
    <a href="/" style="text-align: center">Go To Homepage</a>
</div>
@endsection