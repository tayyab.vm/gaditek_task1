@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-md-6 offset-md-3">
        @if (Session::has('error'))
        <div class="alert alert-danger" role="alert">
          {{Session::get('error')}}
        </div>
        @endif
        <h4 style="text-align: center">A code has been sent to your email address. Please enter that code to confirm your order.</h4>
        <form action="{{route('checkout.confirm')}}" method="post">
            @csrf
            <input type="hidden" value="{{$order_id}}" name="order_id">
            <input type="text" class="form-control" name="code">
            @error('code')
            <small class="text-danger">{{$message}}</small>
            @enderror
            <br>
            <input type="submit"  value="Verify Code" class="btn btn-success">
        </form>
    </div>
</div>
@endsection