@extends('layouts.master')

@section('content')
<div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
    <img src="{{asset('images/order_success.png')}}" alt="" style="width:40%;margin:auto">
</div>
<div class="row" style="text-align: center">
    <h4 >Order Placed Successfuly</h4>
    <h5>Order id : {{$order->id}}</h5>
    <h5>Order status : {{$order->status}}</h5>
    <a href="/" style="text-align: center">Go To Homepage</a>
</div>
@endsection