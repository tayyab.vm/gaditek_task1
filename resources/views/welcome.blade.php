@extends('layouts.master')
@section('page_title')
Services
@endsection
@section('content')
<div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
  @foreach ($services as $service)
  <div class="col">
    <div class="card">
      <img src="{{asset('images/'.$service->image)}}" class="card-img-top" alt="...">
      <div class="card-body">
        <h5 class="card-title">{{$service->name}}</h5>
        <h6 class="price" style="color:rgb(7, 68, 7)">${{$service->price}}</h6>
        <p class="card-text">{{substr($service->description,0,30)}}...</p>
        <a href="{{route('services.show',$service->id)}}" class="btn btn-success">Buy Now</a>
      </div>
    </div>
  </div>
  @endforeach

</div>
@endsection