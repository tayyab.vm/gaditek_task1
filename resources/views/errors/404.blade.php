@extends('layouts.master')

@section('content')
<div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
    <img src="{{asset('images/404.jpg')}}" alt="" style="width:50%;margin:auto">
</div>
<div class="row">
    <a href="/" style="text-align: center">Go To Homepage</a>
</div>
@endsection