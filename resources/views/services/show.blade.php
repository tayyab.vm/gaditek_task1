@extends('layouts.master')

@section('page_title')
Product Detail
@endsection
@section('content')
<div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
  <div class="col-md-4">
    <div class="card">
      <img src="{{asset('images/1.jpg')}}" class="card-img-top" alt="...">
      
    </div>
  </div>
  <div class="col-md-8 product_details">
    <h5 class="card-title">{{$service->name}}</h5>
    <p class="card-text">{{$service->description}}</p>
    <h4 class="card-text">${{$service->price}}</h4>
    <form action="{{route('checkout.show')}}">
      <input type="hidden" value="{{$service->id}}" name="service_id">
      <input type="number" name="quantity" placeholder="Quantity" value="1">
      <br>
      @error('quantity')
        <small class="text-danger">{{$message}}</small>
      @enderror
      <br>
      <p><input type="submit" value="Proceed" class="btn btn-success"></p>
    </form>
  </div>
</div>
@endsection