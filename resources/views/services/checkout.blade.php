@extends('layouts.master')

@section('content')

<div class="py-5 text-center">
  <img class="d-block mx-auto mb-4" src="{{asset('images/cart.png')}}" alt="" width="72" height="57">
  <h2><u>Checkout Details</u></h2>
  @if (Session::has('error'))
  <div class="alert alert-danger" role="alert">
    {{Session::get('error')}}
  </div>
  @endif
</div>
<form action="{{route('checkout.show')}}" method="post">
  @csrf
  <input type="hidden" name="service_id" value="{{$service->id}}">
  <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
    <div class="col-md-5 col-lg-4 order-md-last">
      <h4 class="d-flex justify-content-between align-items-center mb-3">
        <span class="text-primary">Order Details</span>
      </h4>
      <ul class="list-group mb-3">
        <li class="list-group-item d-flex justify-content-between bg-light">
          <div class="text-success">
            <h6 class="my-0">{{$service->name}}</h6>
            <small>Quantity : {{$quantity}}</small>
          </div>
          <span class="text-success">${{$service->price}}</span>
        </li>
        <li class="list-group-item d-flex justify-content-between">
          <span>Total</span>
          <strong>${{$service->price * $quantity}}</strong>
          <input type="hidden" name="total_amount" value="{{$service->price * $quantity}}">
        </li>
      </ul>
      <input type="submit" class="btn btn-success form-control" value="Checkout">
    </div>
    <div class="col-md-7 col-lg-8">
      <h4 class="mb-3"><u>Billing Address</u></h4>
      <div class="row g-3">
        <div class="col-4">
          <label for="firstName">Name</label>
          <input type="text" class="form-control" name="name" value="{{old('name')}}" >
          @error('name')
          <small class="text-danger">{{$message}}</small>
          @enderror
        </div>
        
        <div class="col-4">
          <label for="lastName">Email</label>
          <input type="text" class="form-control" name="email" value="{{old('email')}}">
          @error('email')
          <small class="text-danger">{{$message}}</small>
          @enderror
        </div>
        
        <div class="col-4">
          <label for="lastName">Phone</label>
          <input type="number" class="form-control" value="{{old('phone')}}" name="phone">
          @error('phone')
          <small class="text-danger">{{$message}}</small>
          @enderror
        </div>
        
        <div class="col">
          <label for="address">Address</label>
          <input type="text" class="form-control" value="{{old('address')}}" name="address">
          @error('address')
          <small class="text-danger">{{$message}}</small>
          @enderror
        </div>
        
        
        <h4 class="mb-3"><u>Payment</u></h4>
        
        
        <div class="col-6">
          <label for="cc-name">Name on card</label>
          <input type="text" class="form-control" name="name_on_card" value="{{old('name_on_card')}}">
          @error('name_on_card')
          <small class="text-danger">{{$message}}</small>
          @enderror
        </div>
        
        <div class="col-6">
          <label for="cc-number">Card number</label>
          <input type="number" class="form-control" name="card_number" value="{{old('card_number')}}">
          @error('card_number')
          <small class="text-danger">{{$message}}</small>
          @enderror
        </div>
        <div class="container">
          <div class="row">
            <div class="col-6">
              <label for="cc-expiration">Expiration Month</label>
              <input type="number" class="form-control" name="expiration_month" value="{{old('expiration_month')}}">
              @error('expiration_month')
              <small class="text-danger">{{$message}}</small>
              @enderror
            </div>
            
            <div class="col-6">
              <label for="cc-expiration">Expiration Year</label>
              <input type="number" class="form-control" name="expiration_year" value="{{old('expiration_year')}}">
              @error('expiration_year')
              <small class="text-danger">{{$message}}</small>
              @enderror
            </div>
          </div>
        </div>
        <div class="col-6">
          <label for="cc-cvv">CVV</label>
          <input type="number" class="form-control" name="cvv" value="{{old('cvv')}}">
          @error('cvv')
          <small class="text-danger">{{$message}}</small>
          @enderror
        </div>
      </div>
    </div>
  </div>
</form>

@endsection