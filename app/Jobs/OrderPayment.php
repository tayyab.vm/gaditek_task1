<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Stripe\StripeClient;
use App\Mail\OrderConfirmed;

use Mail;
class OrderPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $order;
    private $user;
    private $user_card;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order,$user,$user_card)
    {
        $this->order = $order;
        $this->user = $user;
        $this->user_card = $user_card;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->order;
        $user = $this->user;
        $user_card = $this->user_card;
        $stripe = new StripeClient(env("STRIPE_SECRET_KEY"));
        $card = ['number' => $user_card->number, 'exp_month' => $user_card->expiry_month, 'exp_year' => $user_card->expiry_year, 'cvc' => $user_card->cvv];
        try
        {
            $token = $stripe
                ->tokens
                ->create(['card' => $card, ]);
        }
        catch(\Exception $exception)
        {
            $order->status = 'failed';
            $order->payment_status = $exception;
            $order->update();
            \Log::alert('Payment Failed',['Payment Failed against order number '.$this->order->id]);
            die;
        }


        $payment = $stripe
        ->charges
        ->create(['amount' => $order->amount * 100, 'currency' => 'usd', 'source' => $token, 'description' => 'Payment from [ ' . $user->name . ' | ' . $user->email . ' ] agains order number ' . $order->id]);
        $order->payment_status = $payment->status;

        if ($payment->status === "succeeded")
        {
            $order->status = 'processing';
            $order->update();

            Mail::to($user->email)
            ->queue(new OrderConfirmed($order));
    
        }
        else
        {
            $order->status = 'failed';
            $order->payment_status = $exception;
            $order->update();
            \Log::alert('Payment Failed',['Payment Failed against order number '.$this->order->id]);
        }

    }
}
