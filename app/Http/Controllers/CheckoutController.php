<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CheckoutRequest;
use App\Models\Service;
use App\Models\User;
use App\Models\Order;
use App\Models\UserCard;
use App\Mail\VerifyOrder;
use App\Jobs\OrderPayment;
use Mail;

class CheckoutController extends Controller
{

    public function index(){
        return view('services.checkout');
    }

    public function show(){
        request()->validate([
            'service_id'                    => 'required|numeric',
            'quantity'                      => 'required|numeric'
        ]);

        $service                            = Service::findOrFail(request()->service_id);
        return view('services.checkout')->with('service',$service)->with('quantity',request()->quantity);
    }

    public function store(CheckoutRequest $request){
        try{
            $user                           = User::where('email',$request->email)->first();
            if(!$user){
                $user                       = new User;
                $user->name                 = $request->name;
                $user->email                = $request->email;
                $user->save();
            }

            $user_card = UserCard::where('number',$request->card_number)->first();
            if(!$user_card){
                $user_card                   = new UserCard;
            }
            $user_card->name_on_card     = $request->name_on_card;
            $user_card->number           = $request->card_number;
            $user_card->expiry_month     = $request->expiration_month;
            $user_card->expiry_year      = $request->expiration_year;
            $user_card->cvv              = $request->cvv;
            $user_card->save();
            

            $verification_code = rand(100000,900000);
            $order = new Order;
            $order->user_id                 = $user->id;
            $order->service_id              = $request->service_id;
            $order->card_id                 = $user_card->id;
            $order->amount                  = $request->total_amount;
            $order->status                  = 'pending';
            $order->phone                   = $request->phone;
            $order->address                 = $request->address;
            $order->verification_code       = $verification_code;
            $order->save();
        }catch(\Exception $e){
            return $e;
            \Session::flash('error','Something Went Wrong! Please Try Again.');
            return back();
        }

        Mail::to($user->email)
            ->queue(new VerifyOrder($order));
            
        return redirect()->route('checkout.verify',['order_id'=>$order->id]);
    }

    public function verify($order_id){
        return view('orders.verify')->with('order_id',$order_id);
    }

    public function confirm(){

        request()->validate([
            'order_id'            => 'required|numeric',
            'code'                => 'required|numeric'
        ]);
        $order = Order::find(request()->order_id);
        if(!$order){
            \Session::flash('error','Invalid Verification Code.');
            return back();
        }
        $user = User::find($order->user_id);
        if(!$user){
            \Session::flash('error','Something Went Wrong.');
            return back();
        }
        $user_card = UserCard::find($order->card_id);
        if(!$user_card){
            \Session::flash('error','Something Went Wrong.');
            return back();
        }
        if($order->verification_code != request()->code){    
            \Session::flash('error','Invalid Verification Code.');
            return back();
        }

        $order->status = 'verified';
        $order->update();
        OrderPayment::dispatch($order,$user,$user_card);
       
        return view('orders.success')->with('order',$order);

    }
}
