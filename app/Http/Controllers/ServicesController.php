<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
class ServicesController extends Controller
{

    public function index(){
        return view('welcome')->with('services',Service::take(10)->get());
    }


    public function show(Service $service){
        return view('services.show')->with('service',$service);
    }
}
