<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
class OrderController extends Controller
{
    public function track(){
        $order = Order::findOrFail(request()->order_id);
        return view('orders.track')->with('order',$order);
    }
}
