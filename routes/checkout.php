<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('checkout','\App\Http\Controllers\CheckoutController@show')->name('checkout.show');
Route::post('checkout','\App\Http\Controllers\CheckoutController@store')->name('checkout.store');
Route::get('checkout/verify/{order_id}','\App\Http\Controllers\CheckoutController@verify')->name('checkout.verify');
Route::post('checkout/conirm','\App\Http\Controllers\CheckoutController@confirm')->name('checkout.confirm');